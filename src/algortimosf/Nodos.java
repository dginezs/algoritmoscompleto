/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algortimosf;

/**
 *
 * @author Diego Ginez
 */
public class Nodos {
    public int id;
	public int grado;
        public double x;
        public double y;
        public boolean visited;
        private boolean Falso;
        private double w;
        public int connected;
        
    public Nodos(){
        this.grado=0;
        this.x=0;
        this.y=0;
        this.connected=0;
        this.Falso=false;
        this.w=0;
    }
    public Nodos(Nodos uso1){
        this.grado=uso1.getCoordenadas();
        this.id=uso1.get_id();
        this.x=uso1.getx();
        this.y=uso1.gety();
        this.connected=uso1.getconnected();
        this.Falso=uso1.getFalso();
        this.w=uso1.getw();
    }
    public Nodos (int aleatorio){    
        this.id = aleatorio;
        this.grado = 0;
        this.x =0;
        this.y =0;
        this.connected=0;
        this.Falso=false;
        this.w=0;
    }
    public Nodos (int aleatorio, double x1, double y1){
        this.id= aleatorio;
        this.grado=0;
        this.x=x1;
        this.y=y1;
        this.Falso=false;
        this.connected =0;
        this.w=0;
    }
    public Nodos (int aleatorio, int con, int gra, double x1, double y1, boolean g, double p){    
        this.id = aleatorio;
        this.connected=con;
        this.grado = gra;
        this.x =x1;
        this.y =y1;
        this.Falso=g;
        this.w=p;
    }
    public void colocar_linea (Nodos n1){
        this.grado=n1.getCoordenadas();
        this.id=n1.get_id();
        this.x=n1.getx();
        this.y=n1.getx();
    }
    public void copiar (Nodos n1){
        this.grado=n1.getCoordenadas();
        this.id=n1.get_id();
        this.x=n1.getx();
        this.y=n1.gety();
        this.connected=n1.getconnected();
        this.Falso=n1.getFalso();
        this.w=n1.getw();
    }
 /////////GETTERS Y SETTERS
   
    public int get_id(){
        return this.id;
    }
    public void AumCoor(int i){
        this.grado=this.grado+i;
    }
    public void setCoordenadas (int i){
        this.grado = i;
    }
    public int getCoordenadas (){
        return this.grado;
    }
    public void set (int idAzar){
        this.id=idAzar;
    }
    public double getx(){
        return this.x;
    }
    public void setx (double x1){
        this.x=x1;
    }
    public double gety(){
        return this.y;
    }
    public void sety (double y1){
        this.y=y1;
    }
    public void setvisited(boolean visited){
           this.visited= visited;
    }
    public boolean getvisited(){
          return this.visited;
    }
    public void conectar(){
        this.connected=1;
    }
    public int getconnected(){
        return this.connected;
    }
    public void setFalso(boolean a){
        this.Falso=a;
    }
    public boolean getFalso(){
        return this.Falso;
    }
    public void setw(double p){
        this.w=p;
    }
    public double getw (){
        return this.w;
    }
}
