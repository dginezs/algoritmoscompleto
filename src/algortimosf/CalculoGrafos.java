/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algortimosf;

import java.util.HashMap;
import java.util.Stack;

/**
 *
 * @author Diego Ginez
 */
public class CalculoGrafos {
    
    private HashMap<Integer,Nodos>nodo;
    private HashMap<Integer,Aristas>arista;
    private HashMap<Integer,Stack>Pila;
    int NumAristas;
    int NumNodos;
    Imprimir I = new Imprimir();
    
    public HashMap<Integer, Stack> getPila() {
        return Pila;
    }

    public void setPila(HashMap<Integer, Stack> Pila) {
        this.Pila = Pila;
    }

    public HashMap<Integer, Nodos> getNodo() {
        return nodo;
    }
    public void setNodo(HashMap<Integer, Nodos> nodo) {
        this.nodo = nodo;
    }
   
    public HashMap<Integer, Aristas> getArista() {
        return arista;
    }

    public void setArista(HashMap<Integer, Aristas> arista) {
        this.arista = arista;
    }

    public int getNumAristas() {
        return NumAristas;
    }

    public void setNumAristas(int NumAristas) {
        this.NumAristas = NumAristas;
    }

    public int getNumNodos() {
        return NumNodos;
    }

    public void setNumNodos(int NumNodos) {
        this.NumNodos = NumNodos;
    }

     public void setNodos (HashMap<Integer,Nodos> a){
        this.nodo=a;
    }
    public void setAristas (HashMap<Integer,Aristas> a){
        this.arista=a;
    }
    
    public void setG (HashMap<Integer,Nodos> a, HashMap<Integer,Aristas> b){
        this.nodo = (HashMap)a;
        this.arista = (HashMap)b;
    }
    
    public CalculoGrafos(HashMap<Integer, Nodos> nodo, HashMap<Integer, Aristas> arista) {
        this.nodo = new HashMap();
        for (int i=0;i<nodo.size();i++){
            this.nodo.put(i, new Nodos(nodo.get(i)));
        }
        this.arista = new HashMap();
        for (int i=0;i<arista.size();i++){
            this.arista.put(i, new Aristas(arista.get(i)));
        }       
    }
    
    public CalculoGrafos(){
        this.nodo=new HashMap();
        this.arista=new HashMap();
    }
    public CalculoGrafos (CalculoGrafos g){
        this.nodo=new HashMap();
        for (int i =0; i< g.getNodo().size();i++){
            this.nodo.put(i, new Nodos(g.getNodo().get(i)));
        }
        this.arista=new HashMap();
        for(int i =0; i<g.getArista().size();i++){
            this.arista.put(i, new Aristas(g.getArista().get(i)));
        }
    }
    public CalculoGrafos Valores (double min, double max){
       
        int NumAristas = this.arista.size();
        for (int i = 0; i < NumAristas; i++) {
            this.arista.get(i).set_p(Math.random()*(max-min)+min);
        }
        CalculoGrafos G = new CalculoGrafos(this.nodo,this.arista);
        return G;
    }
}