/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algortimosf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Stack;

/**
 *
 * @author Diego Ginez
 */
public class EjecuciónAlgoritmos {
    private HashMap<Integer,Nodos>nodo;
    private HashMap<Integer,Aristas>arista;
    private HashMap<Integer,Stack>Pila;
    
    
    /*************************************************************************************/
     public static CalculoGrafos BFS(CalculoGrafos g, Nodos nodoInicial,String nameAux){
        String name=nameAux;
        CalculoGrafos G = new CalculoGrafos(g.getNodo(),g.getArista());
        HashMap<Integer,Nodos> Vertice = new HashMap();
        HashMap<Integer,Aristas> Arista = new HashMap();
        CalculoGrafos A = new CalculoGrafos();
        Imprimir I = new Imprimir();
        HashMap<Integer,HashMap> CapaPrincipal = new HashMap();
        HashMap<Integer,Nodos> SubCapa1 = new HashMap();
        HashMap<Integer,Nodos> SubCapa2 = new HashMap();
        
        int numL = 0,aux = 0,cv=0;
       /* G.getNodo().get(0).setvisited(true);
        SubCapa1.put(0, G.getNodo().get(0));*/
        
        G.getNodo().get(nodoInicial.get_id()).setFalso(true);
        SubCapa1.put(0,G.getNodo().get(nodoInicial.get_id()));
        CapaPrincipal.put(numL,(HashMap)SubCapa1.clone());
        Vertice.put(cv, G.getNodo().get(nodoInicial.get_id()));
        
        
        /*CapaPrincipal.put(numL,(HashMap)SubCapa1.clone());
        Vertice.put(0, G1.getNodo().get(0));*/
        
        for(int x = 0;x<=nodoInicial.get_id();x++){
            SubCapa2.clear();
            int num = -1;
            for(int i = 0;i<SubCapa1.size();i++){
                for(int j = 0;j<G.getArista().size();j++){
                    if(SubCapa1.get(i).get_id()==G.getArista().get(j).get_id1() && G.getNodo().get(G.getArista().get(j).get_id2()).getFalso()==false){
                        G.getNodo().get(G.getArista().get(j).get_id2()).setFalso(true);
                        num++;
                        SubCapa2.put(num, G.getNodo().get(G.getArista().get(j).get_id2()));
                        Arista.put(cv, G.getArista().get(j));
                        cv++;
                        Vertice.put(aux+1, G.getNodo().get(G.getArista().get(j).get_id2()));
                    }
                    if(SubCapa1.get(i).get_id()== G.getArista().get(j).get_id2() && G.getNodo().get(G.getArista().get(j).get_id1()).getFalso()==false){
                       G.getNodo().get(G.getArista().get(j).get_id1()).setFalso(true);
                       num++;
                       SubCapa2.put(num, G.getNodo().get(G.getArista().get(j).get_id1()));                      
                       Arista.put(cv,G.getArista().get(j));
                       cv++;
                       Vertice.put(cv, G.getNodo().get(G.getArista().get(j).get_id1()));
                    }
                }
            }
            numL++;
            SubCapa1=(HashMap)SubCapa2.clone();
            CapaPrincipal.put(numL,(HashMap)SubCapa2.clone());
        }
        for (int i=0; i<g.getNodo().size();i++){
            g.getNodo().get(i).setvisited(false);
        }
        //GrafoCreacion BFS = new GrafoCreacion(Vertice,Arista);
        //BFS.setG(Vertice, Arista);
        I.ImprimirBFS(Vertice, Arista,name);
        //return BFS;
        A.setG(Vertice, Arista);
        return A;
    }
     
   /*************************************************************************************/  
   public static CalculoGrafos DFS_R(CalculoGrafos g, Nodos nodoInicial){    
        HashMap<Integer,Nodos> Vertice = new HashMap();
        HashMap<Integer,Aristas> Arista = new HashMap();
        CalculoGrafos DFSR = new CalculoGrafos(Vertice,Arista);
        CalculoGrafos Regresa;
        Imprimir I = new Imprimir();
             
        boolean[][] AdjMatrix=new boolean[g.getNodo().size()][g.getNodo().size()];
        for (int i = 0; i < g.getArista().size(); i++) {
            AdjMatrix[g.getArista().get(i).get_id1()][g.getArista().get(i).get_id2()]=true;
            AdjMatrix[g.getArista().get(i).get_id2()][g.getArista().get(i).get_id1()]=true;
        }                
        g.getNodo().get(nodoInicial.get_id()).setvisited(true);
        DFSR.getNodo().put(0, new Nodos (g.getNodo().get(nodoInicial.get_id())));   
        for (int i = 0; i < g.getNodo().size(); i++) {
            if(AdjMatrix[nodoInicial.get_id()][i]==true && g.getNodo().get(i).getvisited()==false){
                Regresa=DFS_R(g,g.getNodo().get(i));
                int aux = DFSR.getNodo().size();
                for (int j = 0; j < Regresa.getNodo().size(); j++) {
                        DFSR.getNodo().put(aux+j, Regresa.getNodo().get(j));
                }
                DFSR.getArista().put(DFSR.getArista().size(), new Aristas(nodoInicial.get_id(),i));
                aux=DFSR.getArista().size();
                if (Regresa.getArista().isEmpty()!=true) {
                    for (int j = 0; j < Regresa.getArista().size(); j++) {
                        DFSR.getArista().put(aux+j, Regresa.getArista().get(j));
                    }
                }
            }
        }
        //I.ImprimeBusquedaDFSR(Vertice,Arista);
        return DFSR;
    }
   
/*************************************************************************************/
public static CalculoGrafos DFS_I(CalculoGrafos g, Nodos nodoInicial){
        Stack<Integer>stack = new Stack<>();
        HashMap<Integer,Nodos> Vertice = new HashMap();
        HashMap<Integer,Aristas> Arista = new HashMap();
        CalculoGrafos DFS_I = new CalculoGrafos(Vertice,Arista);
        Imprimir I = new Imprimir();
        int j, x=0;
        boolean Visited;        
        boolean[][] MatriAdj=new boolean[g.getNodo().size()][g.getNodo().size()];
        for (int i = 0; i < g.getArista().size(); i++) {
            MatriAdj[g.getArista().get(i).get_id1()][g.getArista().get(i).get_id2()]=true;
            MatriAdj[g.getArista().get(i).get_id2()][g.getArista().get(i).get_id1()]=true;
        }                
        stack.push(g.getNodo().get(0).get_id());
        g.getNodo().get(0).setvisited(true);
        DFS_I.getNodo().put(x, new Nodos(g.getNodo().get(nodoInicial.get_id())));
             
        while(stack.isEmpty()==false){
            j=stack.peek();
            Visited=false;
            for (int i = 0; i < g.getNodo().size(); i++) {
                if(MatriAdj[j][i]==true && g.getNodo().get(i).getvisited()==false){
                    g.getNodo().get(i).setvisited(true);
                    DFS_I.getArista().put(x, new Aristas(j,i));
                    x++;
                    DFS_I.getNodo().put(x, new Nodos(g.getNodo().get(i)));
                    stack.push(i);
                    Visited=true;
                    i=g.getNodo().size();
                }
                if (i==g.getNodo().size()-1 && Visited==false) {
                    stack.pop();
                }
            }
        }
       for (int i=0; i<g.getNodo().size();i++){
            g.getNodo().get(i).setvisited(false);
        }
        return DFS_I;
    }

/*************************************************************************************/
public static CalculoGrafos Dijkstra(CalculoGrafos g, Nodos nodoInicial){
        for (int i=0; i<g.getNodo().size();i++){
            g.getNodo().get(i).setw(Double.POSITIVE_INFINITY);
        }
        double Matriz[][]= new double[g.getNodo().size()][g.getNodo().size()];
        for(int i=0; i<g.getNodo().size();i++){
            for(int j = 0;j<g.getNodo().size();j++){
                Matriz[i][j] = Double.POSITIVE_INFINITY;
            }
        }
        for(int i=0;i<g.getArista().size();i++){
            Matriz[g.getArista().get(i).get_id1()][g.getArista().get(i).get_id2()] = g.getArista().get(i).get_p();
            Matriz[g.getArista().get(i).get_id2()][g.getArista().get(i).get_id1()] = g.getArista().get(i).get_p();
        }
        ArrayList<Integer> NodosEncontrados = new ArrayList<Integer>();
        HashMap<Integer,Aristas>RecorreIda=new HashMap();
        int numAux=0;
        NodosEncontrados.add(g.getNodo().get(nodoInicial.get_id()).get_id());
        g.getNodo().get(nodoInicial.get_id()).setFalso(true);
        g.getNodo().get(nodoInicial.get_id()).setw(0);
        
        double auxRecorrido=0;
        int a=0;
        int b=0;
        boolean detener= true;
        
        while(auxRecorrido!=Double.POSITIVE_INFINITY){
            auxRecorrido=Double.POSITIVE_INFINITY;
            for(int i=0;i<NodosEncontrados.size();i++){
                for(int j=0;j<g.getNodo().size();j++){
                    if(g.getNodo().get(j).getFalso()!= true && Matriz [NodosEncontrados.get(i)][j]!=Double.POSITIVE_INFINITY){
                        if ((g.getNodo().get(NodosEncontrados.get(i)).getw()+Matriz[NodosEncontrados.get(i)][j]) < auxRecorrido) {
                            g.getNodo().get(j).setw(g.getNodo().get(NodosEncontrados.get(i)).getw()+Matriz[NodosEncontrados.get(i)][j]);
                            auxRecorrido = g.getNodo().get(j).getw();
                            a = NodosEncontrados.get(i);
                            b = j;
                    }
                }
            }
        }
      if (auxRecorrido != Double.POSITIVE_INFINITY){
          g.getNodo().get(b).setFalso(true);
          NodosEncontrados.add(b);
          RecorreIda.put(numAux, new Aristas (a,b,Matriz[a][b]));
          numAux++;          
      }
      }
        HashMap<Integer,Nodos> RecorreVuelta = new HashMap();

        for (int i = 0; i < NodosEncontrados.size(); i++) {
            RecorreVuelta.put(i, new Nodos(g.getNodo().get(NodosEncontrados.get(i))));
        }
        CalculoGrafos G1 = new CalculoGrafos(RecorreVuelta,RecorreIda);
        return G1;               
    }

/*************************************************************************************/
public static CalculoGrafos Kruskal_D(CalculoGrafos g){
        CalculoGrafos G = new CalculoGrafos(g.getNodo(), g.getArista());        
        PriorityQueue<Aristas> ColaPrioridad = new PriorityQueue<>();
        HashMap<Integer, Aristas> MapaPrioridad = new HashMap();
        HashMap<Integer, Nodos> nodo = new HashMap();
        ArrayList<Integer> L = new ArrayList<Integer>();
        int NumAristas = 0;
        int NumNodos = 0;
        double Peso = 0;
        
        for (int i = 0; i < g.getArista().size(); i++) {
            ColaPrioridad.add(g.getArista().get(i));
        }       
        for (int i = 0; i < g.getArista().size(); i++) {
            if (L.contains(ColaPrioridad.peek().get_id1()) && L.contains(ColaPrioridad.peek().get_id2())) {
                CalculoGrafos Cap1 = new CalculoGrafos(g.getNodo(), MapaPrioridad);
                CalculoGrafos Cap2 = new CalculoGrafos();
                Cap2 =DFS_I(Cap1, Cap1.getNodo().get(ColaPrioridad.peek().get_id1()));
                int aux = 0;
                for (int j = 0; j < Cap2.getNodo().size(); j++) {
                    if (Cap2.getNodo().get(j).get_id() == ColaPrioridad.peek().get_id2()) {
                        aux = 1;
                    }
                }
                if (aux == 0) {
                    Peso = Peso + ColaPrioridad.peek().get_p();
                    MapaPrioridad.put(NumAristas, ColaPrioridad.poll());
                    NumAristas++;
                } else {
                    ColaPrioridad.poll();
                }
            } else {
                if (L.contains(ColaPrioridad.peek().get_id1()) == false) {
                    L.add(ColaPrioridad.peek().get_id1());
                }
                if (L.contains(ColaPrioridad.peek().get_id2()) == false) {
                    L.add(ColaPrioridad.peek().get_id2());
                }
                Peso = Peso + ColaPrioridad.peek().get_p();
                MapaPrioridad.put(NumAristas, ColaPrioridad.poll());
                NumAristas++;
            }
            if (MapaPrioridad.size() == g.getNodo().size() - 1) {
                i = g.getArista().size();
            }
        }
        for (int i = 0; i < L.size(); i++) {
            nodo.put(i, g.getNodo().get(L.get(i)));
        }
        System.out.println("Peso de árbol_expancion mínima Kruskal="+Peso);
        CalculoGrafos fin_g = new CalculoGrafos(nodo, MapaPrioridad);
        return fin_g;
        }


/*************************************************************************************/
public static CalculoGrafos PRIM(CalculoGrafos g){
        CalculoGrafos G = new CalculoGrafos(g.getNodo(), g.getArista());
        Aristas Matriz[][] = new Aristas[g.getNodo().size()][g.getNodo().size()];

        for (int i = 0; i < g.getArista().size(); i++) {
            Matriz[g.getArista().get(i).get_id1()][g.getArista().get(i).get_id2()] = g.getArista().get(i);
            Matriz[g.getArista().get(i).get_id2()][g.getArista().get(i).get_id1()] = g.getArista().get(i);
        }
        ArrayList<Integer> NodosEncontrados = new ArrayList<Integer>();
        HashMap<Integer, Aristas> RecorridoIda = new HashMap();
        int numA = 0;
        NodosEncontrados.add(g.getNodo().get(0).get_id());
        g.getNodo().get(0).setFalso(true);
        PriorityQueue<Aristas> pqAristas = new PriorityQueue<>();
        double res = 0;
        for (int z = 0; z < g.getArista().size(); z++) {
            for (int i = 0; i < NodosEncontrados.size(); i++) {
                for (int j = 0; j < g.getNodo().size(); j++) {
                    if (Matriz[NodosEncontrados.get(i)][j] != null && g.getNodo().get(j).getFalso() == false) {
                        pqAristas.add(Matriz[NodosEncontrados.get(i)][j]);
                    }
                }
            }
            if (pqAristas.isEmpty() == false) {
                if (NodosEncontrados.contains(pqAristas.peek().get_id1())) {
                    g.getNodo().get(pqAristas.peek().get_id2()).setFalso(true);
                    NodosEncontrados.add(pqAristas.peek().get_id2());
                    RecorridoIda.put(numA, Matriz[pqAristas.peek().get_id1()][pqAristas.peek().get_id2()]);
                    numA++;
                    res = res + pqAristas.peek().get_p();
                    pqAristas.clear();
                } else {
                    g.getNodo().get(pqAristas.peek().get_id1()).setFalso(true);
                    NodosEncontrados.add(pqAristas.peek().get_id1());
                    RecorridoIda.put(numA, Matriz[pqAristas.peek().get_id1()][pqAristas.peek().get_id2()]);
                    numA++;
                    res = res + pqAristas.peek().get_p();
                    pqAristas.clear();
                }
            }
            if (RecorridoIda.size() == g.getNodo().size() - 1) {
                break;
            }
        }
        System.out.println("Peso árbol_expancion mínima Prim = ."+res);
        CalculoGrafos fin_g = new CalculoGrafos(g.getNodo(), RecorridoIda);
        return fin_g;
    }
}
