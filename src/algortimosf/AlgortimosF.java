/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algortimosf;

import java.util.Scanner;

/**
 *
 * @author Diego Ginez
 */
public class AlgortimosF {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        double Coordenada;
        Imprimir Imp = new Imprimir();
        String res = null;
        GrafosPrincipales gp = new GrafosPrincipales();
       do{
       System.out.println("**************ALGORITMOS EN GRAFOS***********");
       Scanner sc = new Scanner(System.in);
       System.out.println("1. Sólo generación grafos");
       System.out.println("Las siguientes opciones generarán los grafos e implementarán el método sleccionado");
       System.out.println("los parámetros que se soliciten funcionarán y se usarán para el método correspondiente.\n");
       System.out.println("2. Algoritmos BFS y DFS");
       System.out.println("3. Algoritmo Dijkstra");
       System.out.println("4. Algoritmos Kruskal y Prim\n");
       
       System.out.println("Seleccione una opción:");
       int opc=sc.nextInt();
       

       switch (opc){
           case 1:
                System.out.println("1. Modelo Gn,m de Erdös y Rényi");
                System.out.println("2. Modelo Gn,p de Gilbert");
                System.out.println("3. Modelo Gn,r geográfico simple");
                System.out.println("4. Variante del modelo Gn,d Barabási-Albert\n");
                System.out.println("Seleccione una opción:");
                int opc2=sc.nextInt();
                switch(opc2){
                    case 1:
                        System.out.println("***********MODELO ERDOS Y RENYI***********");
                        System.out.println("Indica el número de vértices:");
                        int v=sc.nextInt();
                        System.out.println("Indica el número de aristas");
                        int a=sc.nextInt();
                        gp.GrafosErdos(v,a);
                         System.out.println("¿Desea continuar?(n/y)");
                         res = sc.next();
                         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                    break;
                    case 2:
                        System.out.println("***********MODELO GILBERT***********");
                        System.out.println("Indica el número de vértices:");
                        int vg=sc.nextInt();
                        System.out.println("Indica la probabilidad de creación");
                        float p=sc.nextFloat();
                        gp.GrafosGilbert(vg,p); 
                        System.out.println("¿Desea continuar?(n/y)");
                         res = sc.next();
                         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                    break;
                    case 3:
                        System.out.println("***********MODELO GEOGRAFICO SIMPLE***********");
                        System.out.println("Indica el número de vértices:");
                        int vgeo=sc.nextInt();
                        System.out.println("Indica la distancia entre vertices (entre 0 y 1):");
                        float dist=sc.nextFloat();
                        gp.GrafosGeo(vgeo,dist);
                        System.out.println("¿Desea continuar?(n/y)");
                         res = sc.next();
                         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                    break;
                    case 4:
                        System.out.println("***********MODELO VARIANTE BARABÁSI-ALBERT***********");
                        System.out.println("Indica el número de vértices:");
                        int vBara= sc.nextInt();
                        System.out.println("Indica el número máximo de grado por vértice:");
                        int Grado=sc.nextInt();
                        gp.GrafosBara(vBara,Grado);
                        System.out.println("¿Desea continuar?(n/y)");
                         res = sc.next();
                         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                    break;
                    
                    default:
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                        System.out.println("Entrada inválida\n");
                        res="y";
                    break;
                    
                }
           break;
           case 2:
                
                System.out.println("1. BFS");
                System.out.println("2. DFS");
                
                System.out.println("Seleccione una opción:");
                int opc3= sc.nextInt();
                switch(opc3){
                    case 1:
                    System.out.println("***********BFS***********");
                    System.out.println("Indica el número de vértices:");
                    int vBFS=sc.nextInt();
                    System.out.println("Indica el número de aristas");
                    int aBFS=sc.nextInt();
                    System.out.println("Indica la probabilidad:");
                    float pBFS=sc.nextFloat();
                    System.out.println("Indica la distancia entre vértices(entre 0 y 1)");
                    float distBFS=sc.nextFloat();;
                    System.out.println("Indica el número de grado máximo:");
                    int GradoBFS=sc.nextInt();
                    System.out.println("Indica el nodo inicial(entre 1 y el número de vértices ingresados:");
                    int NodIni=sc.nextInt();
                    String Er="ErdosBFS";
                    CalculoGrafos gE=GrafosPrincipales.GrafosErdos(vBFS,aBFS);
                    CalculoGrafos bfs_Erd = EjecuciónAlgoritmos.BFS(gE, gE.getNodo().get(NodIni),Er);
                    String Gil="GilbertBFS";
                    CalculoGrafos gG=GrafosPrincipales.GrafosGilbert(vBFS,pBFS);
                    CalculoGrafos bfs_Gilbert = EjecuciónAlgoritmos.BFS(gG, gG.getNodo().get(NodIni),Gil);
                    String Geo="GeográficoBFS";
                    CalculoGrafos gGeo=GrafosPrincipales.GrafosGeo(vBFS,distBFS);
                    CalculoGrafos bfs_Geo = EjecuciónAlgoritmos.BFS(gGeo, gGeo.getNodo().get(NodIni),Geo);
                    String Bara="BarabasiBFS";
                    CalculoGrafos gBara=GrafosPrincipales.GrafosBara(vBFS,GradoBFS);
                    CalculoGrafos bfs_Bara = EjecuciónAlgoritmos.BFS(gG, gG.getNodo().get(NodIni),Bara);
                    System.out.println("¿Desea continuar?(n/y)");
                         res = sc.next();
                         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                    break;
                    case 2:
                        System.out.println("***********DFS***********");
                        System.out.println("1. Recursivo");
                        System.out.println("2. Iterativo");
                        
                        System.out.println("Seleccione una opción:");
                        int opc4=sc.nextInt();
                        switch(opc4){
                            case 1:
                                System.out.println("***********DFS RECUERSIVO***********");
                                System.out.println("Indica el número de vértices:");
                                int vDFS_R=sc.nextInt();
                                System.out.println("Indica el número de aristas");
                                int aDFS_R=sc.nextInt();
                                System.out.println("Indica la probabilidad:");
                                float pDFS_R=sc.nextFloat();
                                System.out.println("Indica la distancia entre vértices(entre 0 y 1)");
                                float distDFS_R=sc.nextFloat();
                                System.out.println("Indica el número de grado máximo:");
                                int GradoDFS_R=sc.nextInt();
                                System.out.println("Indica el nodo inicial(entre 1 y el número de vértices ingresados:");
                                int NodIniDFS_R=sc.nextInt();
                                
                                String Er2="ErdosDFS_R";
                                CalculoGrafos gE_dfs=GrafosPrincipales.GrafosErdos(vDFS_R,aDFS_R);
                                CalculoGrafos dfs_Erd = EjecuciónAlgoritmos.DFS_R(gE_dfs, gE_dfs.getNodo().get(NodIniDFS_R));
                                Imp.Imprime(Er2, dfs_Erd);
                                
                                String Gil2="GilbertDFS_R";
                                CalculoGrafos gG_dfs=GrafosPrincipales.GrafosGilbert(vDFS_R,pDFS_R);
                                CalculoGrafos dfs_Gil = EjecuciónAlgoritmos.DFS_R(gG_dfs, gG_dfs.getNodo().get(NodIniDFS_R));
                                Imp.Imprime(Gil2, dfs_Gil);
                                
                                String Geo2="GeográficoDFS_R";
                                CalculoGrafos gGeo_dfs=GrafosPrincipales.GrafosGeo(vDFS_R,distDFS_R);
                                CalculoGrafos dfs_Geo = EjecuciónAlgoritmos.DFS_R(gGeo_dfs, gGeo_dfs.getNodo().get(NodIniDFS_R));
                                Imp.Imprime(Geo2, dfs_Geo);
                                
                                String Bara2="BarabasiDFS_R";
                                CalculoGrafos gBara_dfs=GrafosPrincipales.GrafosBara(vDFS_R,GradoDFS_R);
                                CalculoGrafos dfs_Bara = EjecuciónAlgoritmos.DFS_R(gBara_dfs, gBara_dfs.getNodo().get(NodIniDFS_R));
                                Imp.Imprime(Bara2, dfs_Bara);                          
                                
                                System.out.println("¿Desea continuar?(n/y)");
                         res = sc.next();
                         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                            break;
                            case 2:
                                System.out.println("***********DFS ITERATIVO***********");
                                System.out.println("Indica el número de vértices:");
                                int vDFS_I=sc.nextInt();
                                System.out.println("Indica el número de aristas");
                                int aDFS_I=sc.nextInt();
                                System.out.println("Indica la probabilidad:");
                                float pDFS_I=sc.nextFloat();
                                System.out.println("Indica la distancia entre vértices(entre 0 y 1)");
                                float distDFS_I=sc.nextFloat();
                                System.out.println("Indica el número de grado máximo:");
                                int GradoDFS_I=sc.nextInt();
                                System.out.println("Indica el nodo inicial(entre 1 y el número de vértices ingresados:");
                                int NodIniDFS_I=sc.nextInt();
                                
                                String Er3="ErdosDFS_I";
                                CalculoGrafos gE_dfs_I=GrafosPrincipales.GrafosErdos(vDFS_I,aDFS_I);
                                CalculoGrafos dfsI_Erd = EjecuciónAlgoritmos.DFS_R(gE_dfs_I, gE_dfs_I.getNodo().get(NodIniDFS_I));
                                Imp.Imprime(Er3, dfsI_Erd);
                                
                                String Gil3="GilbertDFS_I";
                                CalculoGrafos gG_dfs_I=GrafosPrincipales.GrafosGilbert(vDFS_I,pDFS_I);
                                CalculoGrafos dfsI_Gil = EjecuciónAlgoritmos.DFS_R(gG_dfs_I, gG_dfs_I.getNodo().get(NodIniDFS_I));
                                Imp.Imprime(Gil3, dfsI_Gil);
                                
                                String Geo3="GeográficoDFS_I";
                                CalculoGrafos gGeo_dfs_I=GrafosPrincipales.GrafosGeo(vDFS_I,distDFS_I);
                                CalculoGrafos dfsI_Geo = EjecuciónAlgoritmos.DFS_R(gGeo_dfs_I, gGeo_dfs_I.getNodo().get(NodIniDFS_I));
                                Imp.Imprime(Geo3, dfsI_Geo);
                                
                                String Bara3="BarabasiDFS_I";
                                CalculoGrafos gBara_dfs_I=GrafosPrincipales.GrafosBara(vDFS_I,GradoDFS_I);
                                CalculoGrafos dfsI_Bara = EjecuciónAlgoritmos.DFS_R(gBara_dfs_I, gBara_dfs_I.getNodo().get(NodIniDFS_I));
                                Imp.Imprime(Bara3, dfsI_Bara);
                                
                                System.out.println("¿Desea continuar?(n/y)");
                         res = sc.next();
                         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                            break;
                            default:
                                System.out.println("Opción inválida");
                            break;
                        }
                    break;
                    default:
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                        System.out.println("Entrada inválida\n");
                        res="y";
                    break;
                }
           break; 
           case 3:
                    System.out.println("***********DIJKSTRA***********");
                    System.out.println("Indica el número de vértices:");
                    int vDIJ=sc.nextInt();
                    System.out.println("Indica el número de aristas");
                    int aDIJ=sc.nextInt();
                    System.out.println("Indica la probabilidad:");
                    float pDIJ=sc.nextFloat();
                    System.out.println("Indica la distancia entre vértices(entre 0 y 1)");
                    float distDIJ=sc.nextFloat();
                    System.out.println("Indica el número de grado máximo:");
                    int GradoDIJ=sc.nextInt();
                    System.out.println("Indica el nodo inicial(entre 1 y el número de vértices ingresados:");
                    int NodIni_Dijk=sc.nextInt();
                    
                    
                    String Er="ErdosDIJKSTRA";
                    CalculoGrafos gE_dij=GrafosPrincipales.GrafosErdos(vDIJ,aDIJ);
                    gE_dij.Valores(0, 100);
                    CalculoGrafos Dij_Erdos = EjecuciónAlgoritmos.Dijkstra(gE_dij,gE_dij.getNodo().get(NodIni_Dijk));
                    Imp.Imprime(Er, Dij_Erdos);
                    
                    String Gil="GilbertDIJKSTRA";
                    CalculoGrafos gG_dij=GrafosPrincipales.GrafosGilbert(vDIJ,pDIJ);
                    gG_dij.Valores(0, 100);
                    CalculoGrafos Dij_Gilbert = EjecuciónAlgoritmos.Dijkstra(gG_dij,gG_dij.getNodo().get(NodIni_Dijk));
                    Imp.Imprime(Gil, Dij_Gilbert);
                    
                    String Geo="GeográficoDIJKSTRA";
                    CalculoGrafos gGeo_dij=GrafosPrincipales.GrafosGeo(vDIJ,distDIJ);
                    gGeo_dij.Valores(0, 100);
                    CalculoGrafos Dij_Geo = EjecuciónAlgoritmos.Dijkstra(gGeo_dij,gGeo_dij.getNodo().get(NodIni_Dijk));
                    Imp.Imprime(Geo, Dij_Geo);
                    
                    String Bara="BarabasiDIJKSTRA";
                    CalculoGrafos gBara_dij=GrafosPrincipales.GrafosBara(vDIJ,GradoDIJ);
                    gBara_dij.Valores(0, 100);
                    CalculoGrafos Dij_Bara = EjecuciónAlgoritmos.Dijkstra(gBara_dij,gBara_dij.getNodo().get(NodIni_Dijk));
                    Imp.Imprime(Bara, Dij_Bara);
                    
                    System.out.println("¿Desea continuar?(n/y)");
                    res = sc.next();
                    System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
           break;
           case 4:
                System.out.println("***********KRUSKAL Y PRIM***********");
                System.out.println("1. Kruskal directo");
                System.out.println("2. Prim");
                
                System.out.println("Seleccione una opción");
                int opc5=sc.nextInt();
                switch(opc5){
                    case 1:
                        System.out.println("***********KRUSKAL DIRECTO***********");
                        System.out.println("Indica el número de vértices:");
                        int vKRUSKAL_D=sc.nextInt();
                        System.out.println("Indica el número de aristas");
                        int aKRUSKAL_D=sc.nextInt();
                        System.out.println("Indica la probabilidad:");
                        float pKRUSKAL_D=sc.nextFloat();
                        System.out.println("Indica la distancia entre vértices(entre 0 y 1)");
                        float distKRUSKAL_D=sc.nextFloat();
                        System.out.println("Indica el número de grado máximo:");
                        int GradoKRUSKAL_D=sc.nextInt();
                        System.out.println("Indica el nodo inicial(entre 1 y el número de vértices ingresados:");
                        int NodIni_Kruskal_D=sc.nextInt();
                        
                        String Er1="ErdosKRUSKAL";
                        CalculoGrafos gErdos_Kruskal=GrafosPrincipales.GrafosErdos(vKRUSKAL_D,aKRUSKAL_D);
                        gErdos_Kruskal.Valores(0, 100);
                        CalculoGrafos Krus_Erdos = EjecuciónAlgoritmos.Kruskal_D(gErdos_Kruskal);
                        Imp.Imprime(Er1, Krus_Erdos);
                        
                        String Gil1="GilbertKRUSKAL";
                        CalculoGrafos gGilbert_Kruskal=GrafosPrincipales.GrafosGilbert(vKRUSKAL_D,pKRUSKAL_D);
                        gGilbert_Kruskal.Valores(0, 100);
                        CalculoGrafos Krus_Gilbert = EjecuciónAlgoritmos.Kruskal_D(gGilbert_Kruskal);
                        Imp.Imprime(Gil1, Krus_Gilbert);
                        
                        String Geo1="GeográficoKRUSKAL";
                        CalculoGrafos gGeo_Kruskal=GrafosPrincipales.GrafosGeo(vKRUSKAL_D,distKRUSKAL_D);
                        gGeo_Kruskal.Valores(0, 100);
                        CalculoGrafos Krus_Geo = EjecuciónAlgoritmos.Kruskal_D(gGeo_Kruskal);
                        Imp.Imprime(Geo1, Krus_Geo);
                        
                        String Bara1="BarabasiKRUSKAL";
                        CalculoGrafos gBarabasi_Kruskal=GrafosPrincipales.GrafosErdos(vKRUSKAL_D,GradoKRUSKAL_D);
                        gBarabasi_Kruskal.Valores(0, 100);
                        CalculoGrafos Krus_Bara = EjecuciónAlgoritmos.Kruskal_D(gBarabasi_Kruskal);
                        Imp.Imprime(Bara1, Krus_Bara);
                  
                        System.out.println("¿Desea continuar?(n/y)");
                         res = sc.next();
                         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                    break;
                    case 2:
                        System.out.println("***********PRIM***********");
                        System.out.println("Indica el número de vértices:");
                        int vPRIM=sc.nextInt();
                        System.out.println("Indica el número de aristas");
                        int aPRIM=sc.nextInt();
                        System.out.println("Indica la probabilidad:");
                        double pPRIM=sc.nextDouble();
                        System.out.println("Indica la distancia entre vértices(entre 0 y 1)");
                        double distPRIM=sc.nextDouble();
                        System.out.println("Indica el número de grado máximo:");
                        int GradoPRIM=sc.nextInt();
                        System.out.println("Indica el nodo inicial(entre 1 y el número de vértices ingresados:");
                        int NodIni_PRIM=sc.nextInt();
                        
                        String Er2="ErdosPRIM";
                        CalculoGrafos gErdos_PRIM=GrafosPrincipales.GrafosErdos(vPRIM,aPRIM);
                        gErdos_PRIM.Valores(0, 50);
                        CalculoGrafos PRIM_Erdos = EjecuciónAlgoritmos.PRIM(gErdos_PRIM);
                        Imp.Imprime(Er2, PRIM_Erdos);
                        
                        System.out.println("¿Desea continuar?(n/y)");
                         res = sc.next();
                         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                    break;
                    default:
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                        System.out.println("Entrada inválida\n");
                        res="y";
                    break;
                }
           break;
           default:
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                System.out.println("Entrada inválida\n");
                res="y";
           break;
       }
       }while(res.equalsIgnoreCase("y"));
       System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
       System.out.println("\tGRACIAS!");
       
    }
    
}
