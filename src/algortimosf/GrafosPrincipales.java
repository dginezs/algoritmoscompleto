/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algortimosf;

import java.util.HashMap;

/**
 *
 * @author Diego Ginez
 */
public class GrafosPrincipales {
    
    public static CalculoGrafos GrafosErdos(int NumNodos, int NumAristas){
         HashMap<Integer,Nodos>nodo;
         HashMap<Integer,Aristas>arista;
        Imprimir I = new Imprimir();
        String name="Erdos";
        
        int NewArist;
	nodo = new HashMap<Integer, Nodos>();
        arista = new HashMap<Integer,Aristas>();
	int ArisCreadas;
        for(int i=0; i< NumNodos; i++){//Creacion de nodos
            nodo.put(i, new Nodos(i));
        }
        //Generacion aristas Aleatorias
        int flag=1;
        
        int arista1=(int)(Math.random()*NumNodos),arista2=(int)(Math.random()*NumNodos);
        arista.put(0,new Aristas(nodo.get(arista1).get_id(),nodo.get(arista2).get_id()));
        //Para autociclos
        while(arista1==arista2 && flag==0){
            arista1=(int)(Math.random()*NumNodos);
            arista2=(int)(Math.random()*NumNodos);
            arista.put(0,new Aristas(nodo.get(arista1).get_id(),nodo.get(arista2).get_id()));
        }       
        nodo.get(arista1).conectar();
        nodo.get(arista2).conectar();
        if(arista1!=arista2){
            nodo.get(arista1).AumCoor(1);
        }
        nodo.get(arista2).AumCoor(1);
        NewArist = 1;//Conteo de aristas
        
        while(NewArist<NumAristas){
            arista1=(int)(Math.random()*NumNodos);
            arista2=(int)(Math.random()*NumNodos);
           
            if(arista1 != arista2 || flag==1){
                int bandera2 = 1;
                int P=0;
                
                while (bandera2==1 && P<NewArist){
                    int a=arista.get(P).get_id1(),b=arista.get(P).get_id2();
                    
                    if((arista1 == a && arista2 ==b)||(arista1 == b && arista2 == a))   bandera2=0;
                        P++;
                 }
                if (bandera2==1){
                    arista.put(NewArist, new Aristas(nodo.get(arista1).get_id(),nodo.get(arista2).get_id()));
                    nodo.get(arista1).conectar();
                    nodo.get(arista2).conectar();
                    if(arista1!=arista2){
                        nodo.get(arista1).AumCoor(1);
                    }
                        nodo.get(arista2).AumCoor(1);
                        NewArist++;                                 
                }
            }
        }
        CalculoGrafos c= new CalculoGrafos(nodo, arista);
        I.Imprimir(nodo,arista,name);
        return c;
    }
    public static CalculoGrafos GrafosGilbert(int NumNodos, float probabilidad){
        HashMap<Integer,Nodos>nodo;
        HashMap<Integer,Aristas>arista;
        Imprimir I = new Imprimir();
        String name="Gilbert";
        int bandera=0;
        int NumAristas=0;
        nodo = new HashMap<Integer, Nodos>();
        arista = new HashMap<Integer,Aristas>();
        
        for(int i=0; i<NumNodos;i++){//Creacion de nodos
            nodo.put(i, new Nodos(i));
        }
        //Union de nodos dependiendo de la probabilidad
        for(int i=0;i<NumNodos;i++){
            for(int k=i;k<NumNodos;k++){
                if(k!=i || bandera==1){
                    if(Math.random()<=probabilidad){
                        arista.put(NumAristas, new Aristas(nodo.get(i).get_id(),nodo.get(k).get_id()));
                        nodo.get(i).conectar();
                        nodo.get(k).conectar();
                          if(i!=k){   nodo.get(i).AumCoor(1);    }
                       nodo.get(k).AumCoor(1);
                        NumAristas++;
                    }
                }
            }
        }
        CalculoGrafos g = new CalculoGrafos(nodo, arista);
        I.Imprimir(nodo, arista,name);
        return g;
    } 
    public static CalculoGrafos GrafosGeo (int NumNodos, float distancia){
        HashMap<Integer,Nodos>nodo;
        HashMap<Integer,Aristas>arista;
        int bandera = 1;
        double dimension=Math.sqrt(2);
        int NumAristas=0;
        Imprimir I = new Imprimir();
        String name="Geográfico";
        nodo = new HashMap<Integer, Nodos>();
        arista = new HashMap<Integer,Aristas>();
	        
	for(int i=0; i<NumNodos; i++){//inicaliza los id de cada nodo
		nodo.put(i, new Nodos(i,Math.random(),Math.random()));
	}
		
		for(int i=0; i<NumNodos; i++){
                    for(int k=i; k<NumNodos; k++){
                        if(k!=i || bandera==1){
                            dimension=Math.sqrt((Math.pow(nodo.get(k).getx()-nodo.get(i).getx(), 2)+Math.pow(nodo.get(k).gety()-nodo.get(i).gety(), 2)));
                            if(dimension<=distancia){
                                arista.put(NumAristas, new Aristas(nodo.get(i).get_id(),nodo.get(k).get_id()));
                                nodo.get(i).AumCoor(1);
                                nodo.get(i).conectar();
                       if(k!=i){
                           nodo.get(k).AumCoor(i);
                           nodo.get(k).conectar();
                       }  NumAristas++;
                            }
                        }
                    }
                } 
                CalculoGrafos g = new CalculoGrafos (nodo, arista);
                I.Imprimir(nodo, arista, name);
                return g;
    }
    public static CalculoGrafos GrafosBara(int NumNodos, int Grado){
        HashMap<Integer,Nodos>nodo;
        HashMap<Integer,Aristas>arista;
        Imprimir I = new Imprimir();
        String name="Barabasi";
        int bandera=0;
        int NumAristas=0;
        //double Coordenada=0;
         nodo = new HashMap<Integer, Nodos>();
        arista = new HashMap<Integer,Aristas>();
	      
        for (int i=0;i<NumNodos;i++){//Creacion de nodos
             nodo.put(i, new Nodos(i));
        }
        //Proceso de colocar vertices en rectangulo y colocar una arista entre cada par que queda a distancia menor del num random generado
        for(int i=0;i<NumNodos;i++){int k=0;
            while(k<=i && nodo.get(i).getCoordenadas()<=Grado){
                if(k !=i || bandera ==1){
                    if(Math.random()<=1-nodo.get(k).getCoordenadas()/Grado){
                        arista.put(NumAristas, new Aristas(nodo.get(i).get_id(),nodo.get(k).get_id()));
                        nodo.get(i).AumCoor(1);
                        nodo.get(i).conectar();
                        /*Vertice nodo1=new Vertice();
                        nodo1.colocar_linea(nodo.get(i));
                        nodo1.AumCoor(1);
                        nodo.put(i, new Vertice (nodo1));*/
                        
                        if(k!=i){
                            nodo.get(k).AumCoor(1);
                            nodo.get(k).conectar();
                        }
                        NumAristas++;
                    }
                }
                k++;
            }
        }
        CalculoGrafos g = new CalculoGrafos(nodo, arista);
       I.Imprimir(nodo, arista, name);
       return g;
      }
}
