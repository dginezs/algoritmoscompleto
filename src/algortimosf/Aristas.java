/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algortimosf;

/**
 *
 * @author Diego Ginez
 */
public class Aristas implements Comparable <Aristas> {
    private int id;
    private int id1;
    private int id2;
    private double w;
    private boolean f;
    
    public Aristas (int id_1, int id_2){
        //this.id=id;
        this.id1=id_1;
        this.id2=id_2;
        this.w=0;
    }
    public Aristas ( int id_1, int id_2, double p){
        //this.id=id;
        this.id1=id_1;
        this.id2=id_2;
        this.w = p;
    }
    public Aristas (Aristas a_ini){
        //this.id=a_ini.get_id();
        this.id1=a_ini.get_id1();
        this.id2=a_ini.get_id2();
        this.w=a_ini.get_p();
    }
    
    /////////GETTERS Y SETTERS
    public int get_id(){
        return this.id;
    }
    public int get_id1(){
        return this.id1;
    }
    public int get_id2(){
        return this.id2;
    }
    public double get_p(){
        return this.w;
    }
    public void set_p(double p){
        this.w=p;
    }
    public void set_id(int KeyNodo){
        this.id=KeyNodo;
    }
    public void set_id1(int KeyNodo){
        this.id1=KeyNodo;
    }
    public void set_id2(int KeyNodo){
    this.id2=KeyNodo;
    }
    public void set_Falso(boolean a){
        this.f=a;
    }
    public boolean get_Falso(){
        return this.f;
    }
    public void Asignar (Aristas a1){
        this.id1=a1.get_id1();
        this.id2=a1.get_id2();
        this.w=a1.get_p();
    }
//override necesario para la implementación de la clase arista
    @Override
    public int compareTo(Aristas A1) {
        if (this.w > A1.get_p())return 1;
        else if(this.w < A1.get_p()) return -1;
        else return 0;
    }

    
}
