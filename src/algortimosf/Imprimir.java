/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algortimosf;

import com.sun.javafx.binding.Logging;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Diego Ginez
 */
public class Imprimir {
    private HashMap<Integer,Nodos>nodo;
    private HashMap<Integer,Aristas>arista;

//Para una visualización más clara, separo las impresiones de los grafos aleatorios y calculados con los distintos
//algoritmos.
    
//Imprimo los grafos generados aleatoriamente.    
    public void Imprimir(HashMap<Integer,Nodos>nodo,HashMap<Integer,Aristas>arista, String name){
         File f = new File(name+".gv");
        String struct = "graph G {\n";
        
        Iterator<HashMap.Entry<Integer, Nodos>> it2 = nodo.entrySet().iterator(); 
        while (it2.hasNext()) {
            struct += it2.next().getValue().id+ ";\n";
        }
        Iterator<HashMap.Entry<Integer, Aristas>> it = arista.entrySet().iterator();
        Iterator<HashMap.Entry<Integer, Aristas>> it3 = arista.entrySet().iterator();
        while (it.hasNext()) {
            struct += it.next().getValue().get_id1()+"--"+it3.next().getValue().get_id2()+";\n";
        }
        PrintWriter pw;
        try {
            pw = new PrintWriter(f);
            pw.write(struct);
            pw.close();
        } catch (FileNotFoundException e) {
            Logging.getLogger();
        }
    }
    //Imprimo los grafos calculados
    public void Imprime(String nombre, CalculoGrafos g){
            FileWriter fichero = null;
            PrintWriter pw = null;
        try{
            fichero = new FileWriter(nombre+".gv");
            pw = new PrintWriter(fichero);
            pw.println("graph G{");
            for(int i=0;i<g.getNodo().size();i++){
                pw.println(g.getNodo().get(i).get_id()+ "  " +"[Label = \""+g.getNodo().get(i).get_id()+" ("+String.format("%.2f", g.getNodo().get(i).getw())+")\"]"+";");
            }
            pw.println();
            for(int i=0;i<g.getArista().size();i++){
               pw.println(g.getArista().get(i).get_id1()+"--"+g.getArista().get(i).get_id2()+ "  " + "[\"" + String.format("%.2f",g.getArista().get(i).get_p())+"\"]"+";");
            }
            pw.println("}");          
        }catch (Exception e){
            e.printStackTrace();
        }finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
        }
    //La implementación de BFS requirio una función de imprimir, distinta al resto.
    public void ImprimirBFS(HashMap<Integer,Nodos>nodo,HashMap<Integer,Aristas>arista,String name){
        File f = new File(name+".gv");
        String struct = "graph G {\n";
        
        Iterator<HashMap.Entry<Integer, Nodos>> it2 = nodo.entrySet().iterator(); 
        while (it2.hasNext()) {
            struct += it2.next().getValue().id+ ";\n";
        }
        Iterator<HashMap.Entry<Integer, Aristas>> it = arista.entrySet().iterator();
        Iterator<HashMap.Entry<Integer, Aristas>> it3 = arista.entrySet().iterator();
        while (it.hasNext()) {
            struct += it.next().getValue().get_id1()+"--"+it3.next().getValue().get_id2()+";\n";
        }
        PrintWriter pw;
        try {
            pw = new PrintWriter(f);
            pw.write(struct);
            pw.close();
        } catch (FileNotFoundException e) {
            Logging.getLogger();
        }
    }
    
}
